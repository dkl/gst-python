#!/usr/bin/python3
# identity.py
# 2019 Daniel Klamt <graphics@pengutronix.de>


# gst-launch-1.0 videotestsrc ! ExampleTransform ! videoconvert ! xvimagesink

import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstBase', '1.0')
gi.require_version('GstVideo', '1.0')

from gi.repository import Gst, GObject, GstBase, GstVideo

import numpy as np

Gst.init(None)
FIXED_CAPS = Gst.Caps.from_string('video/x-raw,format=GRAY8')

class ExampleTransform(GstBase.BaseTransform):
    __gstmetadata__ = ('ExampleTransform Python','Transform',
                      'example gst-python element that can modify the buffer gst-launch-1.0 videotestsrc ! ExampleTransform ! videoconvert ! xvimagesink', 'dkl')

    __gsttemplates__ = (Gst.PadTemplate.new("src",
                                           Gst.PadDirection.SRC,
                                           Gst.PadPresence.ALWAYS,
                                           FIXED_CAPS),
                       Gst.PadTemplate.new("sink",
                                           Gst.PadDirection.SINK,
                                           Gst.PadPresence.ALWAYS,
                                           FIXED_CAPS))
    
    def do_transform_ip(self, buf):
        
        with buf.map(Gst.MapFlags.READ | Gst.MapFlags.WRITE) as info:

            #Get the Caps structure from the buffer
            conf = buf.pool.get_config()
            struct = buf.pool.config_get_params(conf)[1].get_structure(0)
       
            # Get Width 
            i, width = struct.get_int("width")
            if not i:
                print("no width defined")
                return

            # Get Height
            j, height = struct.get_int("height")
            if not j:
                print("no height defined")
                return

            # Now create a NumPy ndarray from the memoryview and modify it:
            if struct.get_string("format") == 'GRAY8':
                A = np.ndarray(shape = (height, width), dtype = np.uint8, buffer = info.data)
                A[20,:] = 255

            else:
                print("Can not work with image format")

            return Gst.FlowReturn.OK

GObject.type_register(ExampleTransform)
__gstelementfactory__ = ("ExampleTransform", Gst.Rank.NONE, ExampleTransform)
